﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace baseDatos.Models
{
    class Tarea
    {
        [PrimaryKey,AutoIncrement,Column("_id")]
        public int Id { get; set; }
        // el nombre no mayor de 150 digitos
        [MaxLength(150)]
        public string Name { get; set; }

        public DateTime DateFinish { get; set; }

        public TimeSpan HourFinsh{ get; set; }

        public Boolean Completed { get; set; }
    }
}
