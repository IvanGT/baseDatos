﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace baseDatos
{
	public partial class App : Application

	{
        // creamos un atributo global para poder accderla de todos los metodos//
        public static string urlbd;
        //cuando el metodo tiene el mismo nombre de la clase.es el contructor//
		public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        public App(string url)
        {
            InitializeComponent();

            urlbd = url;

            MainPage = new NavigationPage(new MainPage());
        }


        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
