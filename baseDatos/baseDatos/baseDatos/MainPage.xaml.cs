﻿using baseDatos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace baseDatos
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        public void Createtask(object sender, EventArgs e)
        {
            // crear objeto del modelo tarea//
            Tarea tarea = new Tarea()
            {
                Name = name.Text,
                DateFinish = dateFinish.Date,
                HourFinsh = timeFinish.Time,
                Completed = false
            };
            // la propieddad using permite validar que no se repitan variables dentro de todo el codigo.//
            // esta linea permite hacer la conexion a la base de datos.
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlbd))
            {
                // crear una tabla en base de datos.
                connection.CreateTable<Tarea>();
                // crear registro  en la tabla "tarea"
                var result = connection.Insert(tarea);
                if (result > 0)

                {
                    DisplayAlert("correcto", "la tarea se creo correctamente", "ok");
                }
                else
                {
                    DisplayAlert("Error", "La Tarea No Fue Creada", "ok");
                }

            }
        }

        async private void ListTaskWindow(object sender, EventArgs e) {
            await Navigation.PushAsync(new ListTask());
        }

    }
}
