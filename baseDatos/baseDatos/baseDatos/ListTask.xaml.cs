﻿using baseDatos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace baseDatos
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListTask : ContentPage
	{
		public ListTask ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlbd))
            {
                List<Tarea> listaTareas;
                listaTareas = connection.Table<Tarea>().ToList();

                listViewTask.ItemsSource = listaTareas;
            }
        }

    }
    
}